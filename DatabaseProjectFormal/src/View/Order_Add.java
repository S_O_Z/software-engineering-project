package View;

import backcode.DatabaseSystemLink;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class Order_Add extends JFrame implements ActionListener {
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JButton addBtn = new JButton("添加订单");
    //
    JButton renewBtn = new JButton("刷新");
    //
    JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

    JButton giveBtn = new JButton("提交");
    //加到成员变量设置为static
    private int pageNow = 1;//当前是第几页
    private int pageSize = 1;//一页显示多少条数据库记录
    String conId;
    String orderId;

    public Order_Add(String ConId, String OrderId) throws Exception {
        this.conId =ConId;
        this.orderId =OrderId;
        initJFrame();
        initJTable();
    }

    private void initJTable() throws Exception {
        Vector<Vector<Object>> data = new Vector<>();
        //数据库调数据
        DatabaseSystemLink.displayOrderData(data,orderId);
        //JTable和tableModel关联后只需要更新tableModel即可更新数据变化反映到JTable
        showOrder studentTableModel1 = showOrder.assembleModel(data);

        JTable jTable = new JTable(studentTableModel1);
        //设置表头
        JTableHeader jTableHeader = new JTableHeader();
        jTableHeader.setFont(new Font(null, Font.BOLD, 16));
        jTableHeader.setForeground(Color.RED);
        //设值表格体
        jTable.setFont(new Font(null, Font.PLAIN, 14));
        jTable.setForeground(Color.BLACK);
        //表格线设置
        jTable.setGridColor(Color.BLACK);
        //表格行高
        jTable.setRowHeight(30);
        //设置多行选择(已经默认2）
        jTable.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        //列头没有了解决：
        JScrollPane jScrollPane = new JScrollPane(jTable);
        //设置表格列的渲染方式
        mainViewRenderRule(jTable);
        this.getContentPane().add(jScrollPane);
    }
    private static void mainViewRenderRule(JTable jTable) {
        Vector<String> columns = showOrder.getColumns();
        for (int i = 0; i < columns.size(); i++) {
            TableColumn column = jTable.getColumn(columns.get(i));
            column.setCellRenderer(new DefaultTableCellRenderer() {
                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    //隔行变色
                    if (row % 2 == 0) setBackground(Color.LIGHT_GRAY);
                    else setBackground(Color.WHITE);
                    //水平居中
                    setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
                    return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
            });
        }
    }

    private void initJFrame() {
        this.setTitle("订单界面");

        northPanel.add(addBtn);
        //
        northPanel.add(renewBtn);
        //

        this.getContentPane().add(northPanel, BorderLayout.NORTH);

        southPanel.add(giveBtn);
        getContentPane().add(southPanel, BorderLayout.SOUTH);

        //添加事件监听
        addBtn.addActionListener(this);

        renewBtn.addActionListener(this);
        giveBtn.addActionListener(this);
        //根据屏幕大小设置主界面
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(new JFrame().getGraphicsConfiguration());
        setBounds(new Rectangle(screenInsets.left, screenInsets.top,
                screenSize.width - screenInsets.left - screenInsets.right,
                screenSize.height - screenInsets.top - screenInsets.bottom));
        //设置窗体充满整个屏幕
        setExtendedState(JFrame.MAXIMIZED_BOTH);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
//加上getter

    @Override
    public void actionPerformed(ActionEvent e) {
        String name = ((JButton) e.getSource()).getText();
        switch (name) {
            case "添加订单":
                new Order_AddDialog(conId, orderId);
                break;
            case "提交":
                //把这组订单传回数据库中
                new Order_PaymentDialog(orderId);
                break;
            //
            case "刷新":
                this.dispose();
                try {
                    new Order_Add(conId,orderId);
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                break;
            //

        }
    }

}

class showOrder extends DefaultTableModel {
    static Vector<String> columns = new Vector<>();

    static {
        columns.addElement("订单编号");
        columns.addElement("商品编号");
        columns.addElement("数量");
    }
    private showOrder() {
        super(null, columns);
    }

    private static showOrder orderTableModel1 = new showOrder();

    public static showOrder assembleModel(Vector<Vector<Object>> data) {
        orderTableModel1.setDataVector(data, columns);
        return orderTableModel1;
    }

    public static Vector<String> getColumns() {
        return columns;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

}