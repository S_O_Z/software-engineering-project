package View;

import backcode.Consumer;
import backcode.DatabaseSystemLink;
import backcode.Tool;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Consumer_Register extends JDialog implements ActionListener {
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    JLabel name = new JLabel("姓名:");
    JTextField nameTxt = new JTextField();
    JLabel phoneNumber = new JLabel("电话：");
    JTextField phoneFiled = new JTextField();
    JButton registerBtn = new JButton("注册");
    JButton exitBtn = new JButton("取消");

    public Consumer_Register() {
        beautifyJDialog();

        centerPanel.add(name);
        centerPanel.add(nameTxt);
        centerPanel.add(phoneNumber);
        centerPanel.add(phoneFiled);
        centerPanel.add(registerBtn);
        centerPanel.add(exitBtn);



        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.setSize(600, 300);
        registerBtn.addActionListener(this);
        exitBtn.addActionListener(this);
        this.setTitle("注册界面");


        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }



    public JTextField getPhoneFiled() {
        return phoneFiled;
    }

    public JTextField getNameTxt() {
        return nameTxt;
    }

    private void beautifyJDialog() {
        //
        name.setFont(new Font("楷体", Font.PLAIN, 20));
        nameTxt.setPreferredSize(new Dimension(200, 30));
        //
        phoneNumber.setFont(new Font("楷体", Font.PLAIN, 20));
        phoneFiled.setPreferredSize(new Dimension(200, 30));
        registerBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        exitBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        //弹窗布局
        Spring childWidth = Spring.sum(Spring.sum(Spring.width(name), Spring.width(nameTxt)),
                Spring.constant(20));
        int offsetX = childWidth.getValue() / 2;

        springLayout.putConstraint(SpringLayout.WEST, name, -offsetX,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, name, 20, SpringLayout.NORTH, centerPanel);
        springLayout.putConstraint(SpringLayout.WEST, nameTxt, 20, SpringLayout.EAST, name);

        springLayout.putConstraint(SpringLayout.NORTH, phoneNumber, 40, SpringLayout.SOUTH, name);
        springLayout.putConstraint(SpringLayout.EAST, phoneNumber, 0, SpringLayout.EAST, name);
        springLayout.putConstraint(SpringLayout.WEST, phoneFiled, 20, SpringLayout.EAST, phoneNumber);
        springLayout.putConstraint(SpringLayout.NORTH, phoneFiled, 0, SpringLayout.NORTH, phoneNumber);

        springLayout.putConstraint(SpringLayout.EAST, registerBtn, 0, SpringLayout.WEST, phoneFiled);
        springLayout.putConstraint(SpringLayout.NORTH, registerBtn, 40, SpringLayout.SOUTH,phoneFiled );
        springLayout.putConstraint(SpringLayout.EAST, exitBtn, offsetX, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, exitBtn, 40, SpringLayout.SOUTH, phoneFiled);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == registerBtn) {
            if (true) {

                String name=this.getNameTxt().getText();
                String phoneNumber=this.getPhoneFiled().getText();
                String id;
                try {
                    id = Tool.getIdNumber(Consumer.IDENTITY);
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                Consumer consumer=new Consumer(id,name,phoneNumber);
                try {
                    if(DatabaseSystemLink.saveConsumerBasicData(consumer)){
                        JOptionPane.showMessageDialog(this,"顾客信息保存成功！");
                        this.dispose();
                    }else {
                        JOptionPane.showMessageDialog(this,"顾客信息保存失败！");
                    }
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            }
        }
        if (e.getSource() == exitBtn) {
            System.out.println("退出");
            this.dispose();
        }
    }
}
