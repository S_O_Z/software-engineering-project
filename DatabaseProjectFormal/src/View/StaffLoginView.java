package View;

import backcode.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class StaffLoginView extends JFrame implements ActionListener {
    SpringLayout springLayout = new SpringLayout();
    JLabel nameLabel = new JLabel("客户订购登记系统", JLabel.CENTER);
    JPanel centerPanel = new JPanel(springLayout);
    JLabel usernameLabel = new JLabel("用户名：");
    JTextField userTxt = new JTextField();
    JLabel passwordLabel = new JLabel("密码：");
    JPasswordField pwdField = new JPasswordField();
    JButton loginBtn = new JButton("登录");
    JButton resetBtn = new JButton("重置");
    JButton registerBtn = new JButton("注册");
    JRadioButton accountantJr = new JRadioButton("会计");
    JRadioButton salesmanJr = new JRadioButton("业务员");

    JPanel jPanel = new JPanel(new GridLayout(1, 2));
    ButtonGroup buttonGroup = new ButtonGroup();

    public StaffLoginView() {
        //设置美化界面
        beautifyFrame();
        //初始化界面
        initFrame();
    }

    private void initFrame() {
        centerPanel.add(usernameLabel);
        centerPanel.add(userTxt);
        centerPanel.add(passwordLabel);
        centerPanel.add(pwdField);
        centerPanel.add(loginBtn);
        centerPanel.add(resetBtn);
        centerPanel.add(registerBtn);
        //单选键
        buttonGroup.add(accountantJr);
        buttonGroup.add(salesmanJr);
        centerPanel.add(jPanel);
        jPanel.add(salesmanJr);
        jPanel.add(accountantJr);
        salesmanJr.setSelected(true);

        //给按钮添加动作监听
        loginBtn.addActionListener(this);
        resetBtn.addActionListener(this);
        registerBtn.addActionListener(this);

        this.getContentPane().add(nameLabel, BorderLayout.NORTH);
        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.setTitle("客户订购登记系统");
        this.setSize(600, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setFocusable(true);
        this.setVisible(true);
        requestFocus();
    }

    private void beautifyFrame() {
        nameLabel.setFont(new Font("华文行楷", Font.PLAIN, 40));
        nameLabel.setPreferredSize(new Dimension(0, 80));
        usernameLabel.setFont(new Font("楷体", Font.PLAIN, 20));
        userTxt.setPreferredSize(new Dimension(200, 30));
        passwordLabel.setFont(new Font("楷体", Font.PLAIN, 20));
        pwdField.setPreferredSize(new Dimension(200, 30));
        loginBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        resetBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        registerBtn.setFont(new Font("楷体", Font.PLAIN, 20));

        //布局
        Spring childWidth = Spring.sum(Spring.sum(Spring.width(usernameLabel), Spring.width(userTxt)),
                Spring.constant(20));
        int offsetX = childWidth.getValue() / 2;
        springLayout.putConstraint(SpringLayout.WEST, usernameLabel, -offsetX,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, usernameLabel, 20, SpringLayout.NORTH, centerPanel);

        springLayout.putConstraint(SpringLayout.WEST, userTxt, 20, SpringLayout.EAST, usernameLabel);
        springLayout.putConstraint(SpringLayout.NORTH, userTxt, 0, SpringLayout.NORTH, usernameLabel);
        springLayout.putConstraint(SpringLayout.NORTH, passwordLabel, 40, SpringLayout.SOUTH, usernameLabel);
        springLayout.putConstraint(SpringLayout.EAST, passwordLabel, 0, SpringLayout.EAST, usernameLabel);
        springLayout.putConstraint(SpringLayout.WEST, pwdField, 20, SpringLayout.EAST, passwordLabel);
        springLayout.putConstraint(SpringLayout.NORTH, pwdField, 0, SpringLayout.NORTH, passwordLabel);

        springLayout.putConstraint(SpringLayout.NORTH, jPanel, 20, SpringLayout.SOUTH, pwdField);
        springLayout.putConstraint(SpringLayout.WEST, jPanel, -offsetX / 2, SpringLayout.HORIZONTAL_CENTER, centerPanel);

        springLayout.putConstraint(SpringLayout.WEST, loginBtn, -offsetX, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, loginBtn, 40, SpringLayout.SOUTH, jPanel);
        springLayout.putConstraint(SpringLayout.EAST, registerBtn, offsetX, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, registerBtn, 40, SpringLayout.SOUTH, jPanel);
        springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, resetBtn, 0, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, resetBtn, 40, SpringLayout.SOUTH, jPanel);
    }

    public JTextField getUserTxt() {
        return userTxt;
    }

    public JPasswordField getPwdField() {
        return pwdField;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton jButton = (JButton) e.getSource();
        String name = jButton.getText();
        switch (name) {
            case "登录":
                try {
                    login();
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                break;
            case "注册":
                new StaffRegisterView();

                break;
            case "重置":
                resetText();
                break;
        }

    }

    /**
     * 重置
     */
    public void resetText() {
        this.getUserTxt().setText("");
        this.getPwdField().setText("");
    }

    /**
     * 登录界面
     *
     * @throws Exception 数据库连接异常
     */
    private void login() throws Exception {
        String username = this.getUserTxt().getText();
        String password = new String(this.getPwdField().getPassword());
        requestFocus();
        //提取数据库的姓名和密码
        if (accountantJr.isSelected()) {
            if (DatabaseSystemLink.staffLogin(username, password, Accountant.IDENTITY)) {
                JOptionPane.showMessageDialog(this, "会计登陆成功");
                this.dispose();
                new AccountantView();
            } else {
                JOptionPane.showMessageDialog(this, "用户名或密码错误");
                resetText();
            }
        } else if (salesmanJr.isSelected()) {
            if (DatabaseSystemLink.staffLogin(username, password, Salesman.IDENTITY)) {
                JOptionPane.showMessageDialog(this, "业务员登陆成功");
                this.dispose();
                new SalesmanView();
            } else {
                JOptionPane.showMessageDialog(this, "用户名或密码错误");
                resetText();
            }
        }
    }

}
