package View;

import backcode.DatabaseSystemLink;
import backcode.Tool;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Consumer_JudgeDialog extends JDialog implements ActionListener {
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);

    JLabel enter = new JLabel("输入顾客姓名：");
    JTextField conNameTxt = new JTextField();
    JButton Add = new JButton("确认");
    JButton exitBtn = new JButton("取消");

    public Consumer_JudgeDialog() {
        beautifyJDialog();
        centerPanel.add(enter);
        centerPanel.add(conNameTxt);
        centerPanel.add(Add);
        centerPanel.add(exitBtn);
        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.setSize(600, 200);
        Add.addActionListener(this);
        exitBtn.addActionListener(this);
        this.setTitle("订单录入");
        this.setAlwaysOnTop(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public JTextField getConNameTxt() {
        return conNameTxt;
    }

    private void beautifyJDialog() {
        enter.setFont(new Font("楷体", Font.PLAIN, 20));
        conNameTxt.setPreferredSize(new Dimension(200, 30));
        Add.setFont(new Font("楷体", Font.PLAIN, 20));
        exitBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        //弹窗布局
        Spring childWidth = Spring.sum(Spring.sum(Spring.width(enter), Spring.width(conNameTxt)),
                Spring.constant(20));
        int offsetx = childWidth.getValue() / 2;

        springLayout.putConstraint(SpringLayout.WEST, enter, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, enter, 20, SpringLayout.NORTH, centerPanel);
        springLayout.putConstraint(SpringLayout.WEST, conNameTxt, 20, SpringLayout.EAST, enter);
        springLayout.putConstraint(SpringLayout.NORTH, conNameTxt, 0, SpringLayout.NORTH, enter);

        springLayout.putConstraint(SpringLayout.EAST, Add, 0, SpringLayout.WEST, conNameTxt);
        springLayout.putConstraint(SpringLayout.NORTH, Add, 40, SpringLayout.SOUTH, conNameTxt);
        springLayout.putConstraint(SpringLayout.EAST, exitBtn, offsetx, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, exitBtn, 40, SpringLayout.SOUTH, conNameTxt);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Add) {
            String name = this.getConNameTxt().getText();
            //判断是否有该顾客,有就进入进入订单界面，没有就创建该顾客
            try {
                if (DatabaseSystemLink.isExistedConsumer(name)) {
                    try {
                        String conId = DatabaseSystemLink.getIdNumberConsumer(name);
                        String orderId = Tool.getOrderIdNumber(conId);
                        if (DatabaseSystemLink.saveOrder(conId, orderId)) {
                            new Order_Add(conId, orderId);
                            this.dispose();
                        }
                    } catch (Exception ex) {
                        throw new RuntimeException(ex);
                    }
                } else {
                    this.dispose();
                    new Consumer_Register();
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        if (e.getSource() == exitBtn) {
            System.out.println("退出");
            this.dispose();
        }
    }
}