package View;


import backcode.DatabaseSystemLink;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Order_SendDialog extends JDialog implements ActionListener {
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);

    JLabel goodNo = new JLabel("发送顾客的姓名：");
    JTextField goodNoTxt = new JTextField();
    JButton send = new JButton("确认");
    JButton exitBtn = new JButton("取消");


    public Order_SendDialog() {
        beautifyJDiolag();

        centerPanel.add(goodNo);
        centerPanel.add(goodNoTxt);
        centerPanel.add(send);
        centerPanel.add(exitBtn);


        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.setSize(600, 200);
        send.addActionListener(this);
        exitBtn.addActionListener(this);
        this.setTitle("补充商品");

        this.setAlwaysOnTop(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    //
    public JTextField getGoodNoTxt() {
        return goodNoTxt;
    }

    //

    private void beautifyJDiolag() {
        goodNo.setFont(new Font("楷体", Font.PLAIN, 20));
        goodNoTxt.setPreferredSize(new Dimension(200, 30));
        send.setFont(new Font("楷体", Font.PLAIN, 20));
        exitBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        //弹窗布局
        Spring childWidth = Spring.sum(Spring.sum(Spring.width(goodNo), Spring.width(goodNoTxt)),
                Spring.constant(20));
        int offsetx = childWidth.getValue() / 2;

        springLayout.putConstraint(SpringLayout.WEST, goodNo, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, goodNo, 20, SpringLayout.NORTH, centerPanel);
        springLayout.putConstraint(SpringLayout.WEST, goodNoTxt, 20, SpringLayout.EAST, goodNo);
        springLayout.putConstraint(SpringLayout.NORTH, goodNoTxt, 0, SpringLayout.NORTH, goodNo);

        springLayout.putConstraint(SpringLayout.EAST, send, 0, SpringLayout.WEST, goodNoTxt);
        springLayout.putConstraint(SpringLayout.NORTH, send, 40, SpringLayout.SOUTH, goodNoTxt);
        springLayout.putConstraint(SpringLayout.EAST, exitBtn, offsetx, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, exitBtn, 40, SpringLayout.SOUTH, goodNoTxt);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == send) {
            String name = this.getGoodNoTxt().getText();
            //点击发送之后判断顾客是否存在
            try {
                if (DatabaseSystemLink.isExistedConsumer(name)) {
                    if (DatabaseSystemLink.isExistedUnSendOrder(name)) {
                        this.dispose();
                        while (DatabaseSystemLink.isExistedUnSendOrder(name)) {
                            new Order_SendView(name);
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "该顾客无为发送订单");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "未找到该顾客");
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        if (e.getSource() == exitBtn) {
            System.out.println("退出");
            this.dispose();
        }
    }
}