package View;

import backcode.DatabaseSystemLink;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class Consumer_ShowWithOperation extends JFrame implements ActionListener {
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JButton addBtn = new JButton("添加");
    JButton deleteBtn = new JButton("修改");
    JTextField searchTxt = new JTextField(15);
    JButton searchBtn = new JButton("查询");
    //
    JButton renewBtn = new JButton("刷新");
    //
    JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    JButton exitBtn = new JButton("返回");

    public Consumer_ShowWithOperation() throws Exception {
        initJframe();
        initJTable();
    }

    private void initJTable() throws Exception {
        Vector<Vector<Object>> data = new Vector<>();
        //数据库调数据
        DatabaseSystemLink.displayConData(data);
        //Jtable和tablemodel关联后只需要更新tablemodel即可更新数据变化反映到JTable
        StudentTableModel3 studentTableModel1 = StudentTableModel3.assembleModel(data);

        JTable jTable = new JTable(studentTableModel1);
        //设置表头
        JTableHeader jTableHeader = new JTableHeader();
        jTableHeader.setFont(new Font(null, Font.BOLD, 16));
        jTableHeader.setForeground(Color.RED);
        //设值表格体
        jTable.setFont(new Font(null, Font.PLAIN, 14));
        jTable.setForeground(Color.BLACK);
        //表格线设置
        jTable.setGridColor(Color.BLACK);
        //表格行高
        jTable.setRowHeight(30);
        //设置多行选择(已经默认2）
        jTable.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        //列头没有了解决：
        JScrollPane jScrollPane = new JScrollPane(jTable);
        //设置表格列的渲染方式
        mainViewrenderRule(jTable);
        this.getContentPane().add(jScrollPane);
    }

    private static void mainViewrenderRule(JTable jTable) {
        Vector<String> columns = StudentTableModel3.getColumns();
        for (int i = 0; i < columns.size(); i++) {
            TableColumn column = jTable.getColumn(columns.get(i));
            column.setCellRenderer(new DefaultTableCellRenderer() {
                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    //隔行变色
                    if (row % 2 == 0) setBackground(Color.LIGHT_GRAY);
                    else setBackground(Color.WHITE);
                    //水平居中
                    setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
                    return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
            });
        }
    }

    private void initJframe() {
        this.setTitle("客户列表主界面");

        northPanel.add(addBtn);
        northPanel.add(deleteBtn);
        northPanel.add(searchTxt);
        northPanel.add(searchBtn);
        //
        northPanel.add(renewBtn);
        //

        this.getContentPane().add(northPanel, BorderLayout.NORTH);

        southPanel.add(exitBtn);
        getContentPane().add(southPanel, BorderLayout.SOUTH);

        //添加事件监听
        addBtn.addActionListener(this);
        deleteBtn.addActionListener(this);
        searchBtn.addActionListener(this);
        renewBtn.addActionListener(this);
        exitBtn.addActionListener(this);
        //根据屏幕大小设置主界面
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(new JFrame().getGraphicsConfiguration());
        setBounds(new Rectangle(screenInsets.left, screenInsets.top,
                screenSize.width - screenInsets.left - screenInsets.right,
                screenSize.height - screenInsets.top - screenInsets.bottom));
        //设置窗体充满整个屏幕
        setExtendedState(JFrame.MAXIMIZED_BOTH);

        Image image = new ImageIcon("src\\main\\resources\\img.png").getImage();
        this.setIconImage(image);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    //加上getter
    public JTextField getSearchTxt() {
        return searchTxt;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String name = ((JButton) e.getSource()).getText();
        switch (name) {
            case "添加":
                new Consumer_Register();
                break;
            case "查询":
                String conName = this.getSearchTxt().getText();
                try {
                    if (DatabaseSystemLink.isExistedConsumer(conName)) {
                        new Consumer_SelectDialog(conName);
                    } else {
                        JOptionPane.showMessageDialog(this, "客户不存在");
                    }
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                break;
            case "修改":
                new Consumer_UpdateDialog();
                break;
            //
            case "刷新":
                this.dispose();
                try {
                    new Consumer_ShowWithOperation();
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                break;
            //
            case "返回":
                this.dispose();
                break;

        }
    }


}

class StudentTableModel3 extends DefaultTableModel {
    static Vector<String> columns = new Vector<>();

    static {
        columns.addElement("客户编号");
        columns.addElement("姓名");
        columns.addElement("电话号码");
        columns.addElement("订单数量");
    }

    private StudentTableModel3() {
        super(null, columns);
    }

    private static StudentTableModel3 studentTableModel1 = new StudentTableModel3();

    public static StudentTableModel3 assembleModel(Vector<Vector<Object>> data) {
        studentTableModel1.setDataVector(data, columns);
        return studentTableModel1;
    }

    public static Vector<String> getColumns() {
        return columns;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}