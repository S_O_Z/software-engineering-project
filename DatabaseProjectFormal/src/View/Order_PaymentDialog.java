package View;


import backcode.DatabaseSystemLink;
import backcode.Order;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Order_PaymentDialog extends JDialog implements ActionListener {

    SpringLayout springLayout = new SpringLayout();
    JLabel nameLabel = new JLabel("支付方式", JLabel.CENTER);
    JPanel centerPanel = new JPanel(springLayout);
    JButton pay1 = new JButton("现  金");
    JButton pay2 = new JButton("信用卡");
    JButton pay3 = new JButton("发  票");
    String orderId;

    public Order_PaymentDialog(String orderId) {
        this.orderId = orderId;
        centerPanel.add(pay1);
        centerPanel.add(pay2);
        centerPanel.add(pay3);
        this.getContentPane().add(nameLabel, BorderLayout.NORTH);
        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.setTitle("支付");
        this.setSize(500, 400);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setFocusable(true);
        this.setVisible(true);
        requestFocus();
        nameLabel.setFont(new Font("行楷", Font.PLAIN, 40));
        pay1.setFont(new Font("楷体", Font.PLAIN, 20));
        pay2.setFont(new Font("楷体", Font.PLAIN, 20));
        Spring childWidth = Spring.sum(Spring.width(pay1),
                Spring.constant(20));
        int offsetx = childWidth.getValue() / 2;
        springLayout.putConstraint(SpringLayout.WEST, pay1, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, pay1, 20, SpringLayout.NORTH, centerPanel);
        springLayout.putConstraint(SpringLayout.WEST, pay2, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, pay2, 60, SpringLayout.SOUTH, pay1);
        springLayout.putConstraint(SpringLayout.WEST, pay3, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, pay3, 60, SpringLayout.SOUTH, pay2);
        pay1.addActionListener(this);
        pay2.addActionListener(this);
        pay3.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String name = ((JButton) e.getSource()).getText();
        String payment = null;
        switch (name) {
            case "现  金":
                payment = Order.payByCash;
                try {
                    if (DatabaseSystemLink.updateOrderPayment(orderId, payment)) {
                        JOptionPane.showMessageDialog(this, "提交成功");

                        this.dispose();
                    }
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                break;
            case "发  票":
                payment = Order.payByCheque;
                try {
                    if (DatabaseSystemLink.updateOrderPayment(orderId, payment)) {
                        JOptionPane.showMessageDialog(this, "提交成功");
                        this.dispose();
                    }
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                break;
            case "信用卡":
                payment = Order.payByCreditCard;
                try {
                    if (DatabaseSystemLink.updateOrderPayment(orderId, payment)) {
                        JOptionPane.showMessageDialog(this, "提交成功");
                        this.dispose();
                    }
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                break;
        }

    }

}

