package View;

import backcode.Consumer;
import backcode.DatabaseSystemLink;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Consumer_UpdateDialog extends JDialog implements ActionListener {
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);

    JLabel consumerNo = new JLabel("需要修改的客户Id：");
    JTextField consumerNoTxt = new JTextField();
    JButton Add = new JButton("确认");
    JButton exitBtn = new JButton("取消");
    public Consumer_UpdateDialog() {
        beautifyJDialog();

        centerPanel.add(consumerNo);
        centerPanel.add(consumerNoTxt);
        centerPanel.add(Add);
        centerPanel.add(exitBtn);


        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.setSize(600, 200);
        Add.addActionListener(this);
        exitBtn.addActionListener(this);
        this.setTitle("更新顾客");
        this.setAlwaysOnTop(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
    public JTextField getConsumerNoTxt() {
        return consumerNoTxt;
    }


    private void beautifyJDialog() {
        consumerNo.setFont(new Font("楷体", Font.PLAIN, 20));
        consumerNoTxt.setPreferredSize(new Dimension(200, 30));
        Add.setFont(new Font("楷体", Font.PLAIN, 20));
        exitBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        //弹窗布局
        Spring childWidth = Spring.sum(Spring.sum(Spring.width(consumerNo), Spring.width(consumerNoTxt)),
                Spring.constant(20));
        int offsetx = childWidth.getValue() / 2;

        springLayout.putConstraint(SpringLayout.WEST, consumerNo, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, consumerNo, 20, SpringLayout.NORTH, centerPanel);
        springLayout.putConstraint(SpringLayout.WEST, consumerNoTxt, 20, SpringLayout.EAST, consumerNo);
        springLayout.putConstraint(SpringLayout.NORTH, consumerNoTxt, 0, SpringLayout.NORTH, consumerNo);

        springLayout.putConstraint(SpringLayout.EAST, Add, 0, SpringLayout.WEST, consumerNoTxt);
        springLayout.putConstraint(SpringLayout.NORTH, Add, 40, SpringLayout.SOUTH, consumerNoTxt);
        springLayout.putConstraint(SpringLayout.EAST, exitBtn, offsetx, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, exitBtn, 40, SpringLayout.SOUTH, consumerNoTxt);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Add) {
            //判断修改的id是否正确
            String id=this.getConsumerNoTxt().getText();
            try {
                if (DatabaseSystemLink.isExistedIdNumber(id, Consumer.IDENTITY)) {
                    new UpdateConJdialog();
                    this.dispose();
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        if (e.getSource() == exitBtn) {
            System.out.println("退出");
            this.dispose();
        }
    }
}
class UpdateConJdialog extends JDialog implements ActionListener {
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);

    JLabel name = new JLabel("姓名：");
    JTextField nameTxt = new JTextField();
    JLabel phoneNumber = new JLabel("电话号码：");
    JTextField phoneNumberTxt = new JTextField();
    JButton Add = new JButton("修改");
    JButton exitBtn = new JButton("取消");
    boolean status = false;


    public UpdateConJdialog() {
        beautifyJDialog();

        centerPanel.add(name);
        centerPanel.add(nameTxt);
        centerPanel.add(phoneNumber);
        centerPanel.add(phoneNumberTxt);
        centerPanel.add(Add);
        centerPanel.add(exitBtn);

        this.setAlwaysOnTop(true);
        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.setSize(600, 300);
        Add.addActionListener(this);
        exitBtn.addActionListener(this);
        this.setTitle("修改商品");

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public JTextField getNameTxt() {
        return nameTxt;
    }

    public JTextField getPhoneNumberTxt() {
        return phoneNumberTxt;
    }

    private void beautifyJDialog() {
        name.setFont(new Font("楷体", Font.PLAIN, 20));
        nameTxt.setPreferredSize(new Dimension(200, 30));
        phoneNumber.setFont(new Font("楷体", Font.PLAIN, 20));
        phoneNumberTxt.setPreferredSize(new Dimension(200, 30));
        Add.setFont(new Font("楷体", Font.PLAIN, 20));
        exitBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        //弹窗布局
        Spring childWidth = Spring.sum(Spring.sum(Spring.width(name), Spring.width(nameTxt)),
                Spring.constant(20));
        int offsetx = childWidth.getValue() / 2;

        springLayout.putConstraint(SpringLayout.WEST, name, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, name, 20, SpringLayout.NORTH, centerPanel);
        springLayout.putConstraint(SpringLayout.WEST, nameTxt, 20, SpringLayout.EAST, name);
        springLayout.putConstraint(SpringLayout.NORTH, nameTxt, 0, SpringLayout.NORTH, name);
        springLayout.putConstraint(SpringLayout.NORTH, phoneNumber, 40, SpringLayout.SOUTH, name);
        springLayout.putConstraint(SpringLayout.EAST, phoneNumber, 0, SpringLayout.EAST, name);
        springLayout.putConstraint(SpringLayout.WEST, phoneNumberTxt, 20, SpringLayout.EAST, phoneNumber);
        springLayout.putConstraint(SpringLayout.NORTH, phoneNumberTxt, 0, SpringLayout.NORTH, phoneNumber);

        springLayout.putConstraint(SpringLayout.EAST, Add, 0, SpringLayout.WEST, phoneNumberTxt);
        springLayout.putConstraint(SpringLayout.NORTH, Add, 40, SpringLayout.SOUTH, phoneNumberTxt);
        springLayout.putConstraint(SpringLayout.EAST, exitBtn, offsetx, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, exitBtn, 40, SpringLayout.SOUTH, phoneNumberTxt);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Add) {


        }
        if (e.getSource() == exitBtn) {
            System.out.println("退出");
            this.dispose();
        }
    }
}
