package View;

import backcode.DatabaseSystemLink;
import backcode.Order;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class Order_SendView extends JFrame implements ActionListener {
    String Id = "订单编号:  ";
    String money = "总金额:  ";
    String pay = "                             支付方式： ";
    String conName;
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

    JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    JButton nextBtn = new JButton("发送");
    //加到成员变量设置为static

    public Order_SendView(String conName) throws Exception {
        this.conName = conName;

        initJTable();
        initJFrame();
    }
    private void initJTable() throws Exception {
        Vector<Vector<Object>> data = new Vector<>();
        //数据库调数据
        Order order = DatabaseSystemLink.displayUnSendOrderData(conName);
        double money = 0;
        for (var g : order.getGoodsArrayList()
        ) {
            Vector<Object> goods = new Vector<>();
            goods.addElement(g.getGoodsIdNumber());
            goods.addElement(g.getGoodsName());
            goods.addElement(g.getGoodsPrice());
            goods.addElement(g.getGoodsAmount());
            data.add(goods);
            money += g.getGoodsPrice() * g.getGoodsAmount();
        }
        this.money += money;
        this.Id += order.getOrderIdNumber();
        this.pay += order.getPayment();
        //JTable和tableModel关联后只需要更新tableModel即可更新数据变化反映到JTable
        StudentTableModel4 studentTableModel1 = StudentTableModel4.assembleModel(data);

        JTable jTable = new JTable(studentTableModel1);
        //设置表头
        JTableHeader jTableHeader = new JTableHeader();
        jTableHeader.setFont(new Font(null, Font.BOLD, 16));
        jTableHeader.setForeground(Color.RED);
        //设值表格体
        jTable.setFont(new Font(null, Font.PLAIN, 14));
        jTable.setForeground(Color.BLACK);
        //表格线设置
        jTable.setGridColor(Color.BLACK);
        //表格行高
        jTable.setRowHeight(30);
        //设置多行选择(已经默认2）
        jTable.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        //列头没有了解决：
        JScrollPane jScrollPane = new JScrollPane(jTable);
        //设置表格列的渲染方式
        mainViewRenderRule(jTable);
        this.getContentPane().add(jScrollPane);
    }

    private static void mainViewRenderRule(JTable jTable) {
        Vector<String> columns = StudentTableModel4.getColumns();
        for (int i = 0; i < columns.size(); i++) {
            TableColumn column = jTable.getColumn(columns.get(i));
            column.setCellRenderer(new DefaultTableCellRenderer() {
                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    //隔行变色
                    if (row % 2 == 0) setBackground(Color.LIGHT_GRAY);
                    else setBackground(Color.WHITE);
                    //水平居中
                    setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
                    return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
            });
        }
    }

    private void initJFrame() {
        JLabel ordId = new JLabel(Id);
        JLabel sumMoney = new JLabel(money);
        JLabel payWay = new JLabel(pay);
        this.setTitle("订单主界面");
        ordId.setFont(new Font("楷体", Font.PLAIN, 20));
        sumMoney.setFont(new Font("楷体", Font.PLAIN, 20));
        payWay.setFont(new Font("宋体", Font.PLAIN, 20));

        northPanel.add(ordId);
        northPanel.add(sumMoney);
        northPanel.add(payWay);


        this.getContentPane().add(northPanel, BorderLayout.NORTH);

        southPanel.add(nextBtn);
        getContentPane().add(southPanel, BorderLayout.SOUTH);

        //添加事件监听
        nextBtn.addActionListener(this);
        //根据屏幕大小设置主界面
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(new JFrame().getGraphicsConfiguration());
        setBounds(new Rectangle(screenInsets.left, screenInsets.top,
                screenSize.width - screenInsets.left - screenInsets.right,
                screenSize.height - screenInsets.top - screenInsets.bottom));
        //设置窗体充满整个屏幕
        setExtendedState(JFrame.MAXIMIZED_BOTH);

        Image image = new ImageIcon("src\\main\\resources\\img.png").getImage();
        this.setIconImage(image);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.getContentPane().repaint();
    }
//加上getter


    @Override
    public void actionPerformed(ActionEvent e) {
        String name = ((JButton) e.getSource()).getText();
        switch (name) {
            case "发送":
                JOptionPane.showMessageDialog(this, "发送成功!");
                this.dispose();
        }
    }

}

class StudentTableModel4 extends DefaultTableModel {
    static Vector<String> columns = new Vector<>();

    static {
        columns.addElement("商品编号");
        columns.addElement("商品名");
        columns.addElement("价格");
        columns.addElement("数量");
    }

    private StudentTableModel4() {
        super(null, columns);
    }

    private static StudentTableModel4 studentTableModel1 = new StudentTableModel4();

    public static StudentTableModel4 assembleModel(Vector<Vector<Object>> data) {
        studentTableModel1.setDataVector(data, columns);
        return studentTableModel1;
    }

    public static Vector<String> getColumns() {
        return columns;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

}