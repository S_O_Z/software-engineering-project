package View;


import backcode.DatabaseSystemLink;
import backcode.Goods;
import backcode.Tool;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Goods_PurchaseDialog extends JDialog implements ActionListener {
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    //
    JLabel Amount = new JLabel("数量: ");
    JTextField AmountTxt = new JTextField();
    //
    JLabel goodsName = new JLabel("商品名：");
    JTextField goodNameField = new JTextField();
    JLabel Price = new JLabel("价格：");
    JTextField PriceTxt = new JTextField();

    JButton Add = new JButton("添加");
    JButton exitBtn = new JButton("取消");


    public Goods_PurchaseDialog() {
        beautifyJDialog();
        //
        centerPanel.add(Amount);
        centerPanel.add(AmountTxt);
        //
        centerPanel.add(goodsName);
        centerPanel.add(goodNameField);
        centerPanel.add(Price);
        centerPanel.add(PriceTxt);

        centerPanel.add(Add);
        centerPanel.add(exitBtn);
        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.setSize(600, 440);
        Add.addActionListener(this);
        exitBtn.addActionListener(this);
        this.setTitle("添加商品");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    //
    public JTextField getGoodNameField() {
        return goodNameField;
    }
    public JTextField getPriceTxt() {
        return PriceTxt;
    }
    public JTextField getAmountTxt() {
        return AmountTxt;
    }
    //

    private void beautifyJDialog() {
        goodsName.setFont(new Font("楷体", Font.PLAIN, 20));
        goodNameField.setPreferredSize(new Dimension(200, 30));
        //
        Amount.setFont(new Font("楷体", Font.PLAIN, 20));
        AmountTxt.setPreferredSize(new Dimension(200, 30));
        //
        Price.setFont(new Font("楷体", Font.PLAIN, 20));
        PriceTxt.setPreferredSize(new Dimension(200, 30));
        Add.setFont(new Font("楷体", Font.PLAIN, 20));
        exitBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        //弹窗布局
        Spring childWidth = Spring.sum(Spring.sum(Spring.width(goodsName), Spring.width(goodNameField)),
                Spring.constant(20));
        int offsetx = childWidth.getValue() / 2;

        springLayout.putConstraint(SpringLayout.WEST, goodsName, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, goodsName, 20, SpringLayout.NORTH, centerPanel);
        springLayout.putConstraint(SpringLayout.WEST, goodNameField, 20, SpringLayout.EAST, goodsName);
        springLayout.putConstraint(SpringLayout.NORTH, goodNameField, 0, SpringLayout.NORTH, goodsName);
        springLayout.putConstraint(SpringLayout.NORTH, Price, 40, SpringLayout.SOUTH, goodsName);
        springLayout.putConstraint(SpringLayout.EAST, Price, 0, SpringLayout.EAST, goodsName);
        springLayout.putConstraint(SpringLayout.WEST, PriceTxt, 20, SpringLayout.EAST, Price);
        springLayout.putConstraint(SpringLayout.NORTH, PriceTxt, 0, SpringLayout.NORTH, Price);
        springLayout.putConstraint(SpringLayout.NORTH, Amount, 40, SpringLayout.SOUTH, Price);
        springLayout.putConstraint(SpringLayout.EAST, Amount, 0, SpringLayout.EAST, Price);
        springLayout.putConstraint(SpringLayout.WEST, AmountTxt, 20, SpringLayout.EAST, Amount);
        springLayout.putConstraint(SpringLayout.NORTH, AmountTxt, 0, SpringLayout.NORTH, Amount);
        //


//        springLayout.putConstraint(SpringLayout.WEST, kjJr,-offsetx, SpringLayout.HORIZONTAL_CENTER,centerPanel);
//        springLayout.putConstraint(SpringLayout.NORTH, kjJr,40,SpringLayout.SOUTH,passwordLabel);
//        springLayout.putConstraint(SpringLayout.EAST, ywJr,offsetx, SpringLayout.HORIZONTAL_CENTER,centerPanel);
//        springLayout.putConstraint(SpringLayout.NORTH, ywJr,40,SpringLayout.SOUTH,passwordLabel);

        springLayout.putConstraint(SpringLayout.EAST, Add, 0, SpringLayout.WEST, Amount);
        springLayout.putConstraint(SpringLayout.NORTH, Add, 40, SpringLayout.SOUTH, AmountTxt);
        springLayout.putConstraint(SpringLayout.EAST, exitBtn, offsetx, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, exitBtn, 40, SpringLayout.SOUTH, AmountTxt);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Add) {
            //点击注册后,选择对应添加到数据库
            String id;
            try {
                id= Tool.getIdNumber(Goods.IDENTITY);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
            String name = this.getGoodNameField().getText();
            String price = this.getPriceTxt().getText();
            String amount = this.getAmountTxt().getText();
            Goods goods = new Goods(id, name, Double.parseDouble(price), Integer.parseInt(amount));
            try {
                if (DatabaseSystemLink.saveGoodsData(goods)) {
                    JOptionPane.showMessageDialog(this, "商品进货成功！");
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(this, "商品进货失败！");
                    this.dispose();
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        if (e.getSource() == exitBtn) {
            this.dispose();
        }
    }
}
