package View;

import backcode.DatabaseSystemLink;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.util.Vector;

public class Consumer_SelectDialog extends JDialog {

    public Consumer_SelectDialog(String name) throws Exception {
        Vector<Vector<Object>> data = new Vector<>();
        //数据库调数据
        DatabaseSystemLink.displayConData(data,name);
        //JTable和tableModel关联后只需要更新tableModel即可更新数据变化反映到JTable
        StudentTableModel5 studentTableModel1 = StudentTableModel5.assembleModel(data);

        JTable jTable = new JTable(studentTableModel1);
        //设置表头
        JTableHeader jTableHeader = new JTableHeader();
        jTableHeader.setFont(new Font(null, Font.BOLD, 16));
        jTableHeader.setForeground(Color.RED);
        //设值表格体
        jTable.setFont(new Font(null, Font.PLAIN, 14));
        jTable.setForeground(Color.BLACK);
        //表格线设置
        jTable.setGridColor(Color.BLACK);
        //表格行高
        jTable.setRowHeight(30);
        //设置多行选择(已经默认2）
        jTable.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        //列头没有了解决：
        JScrollPane jScrollPane = new JScrollPane(jTable);
        //设置表格列的渲染方式
        mainViewrenderRule(jTable);
        //根据屏幕大小设置主界面
        this.setTitle("查询结果");
        this.setSize(1000, 1000);
        this.getContentPane().add(jScrollPane);
        this.setAlwaysOnTop(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
    private static void mainViewrenderRule(JTable jTable) {
        Vector<String> columns = StudentTableModel5.getColumns();
        for (int i = 0; i < columns.size(); i++) {
            TableColumn column = jTable.getColumn(columns.get(i));
            column.setCellRenderer(new DefaultTableCellRenderer() {
                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    //隔行变色
                    if (row % 2 == 0) setBackground(Color.LIGHT_GRAY);
                    else setBackground(Color.WHITE);
                    //水平居中
                    setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
                    return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
            });
        }
    }
}
class StudentTableModel5 extends DefaultTableModel {
    static Vector<String> columns = new Vector<>();

    static {
        columns.addElement("客户编号");
        columns.addElement("客户姓名");
        columns.addElement("电话号码");
        columns.addElement("订单数量");
    }

    private StudentTableModel5() {
        super(null, columns);
    }

    private static StudentTableModel5 studentTableModel1 = new StudentTableModel5();

    public static StudentTableModel5 assembleModel(Vector<Vector<Object>> data) {
        studentTableModel1.setDataVector(data, columns);
        return studentTableModel1;
    }

    public static Vector<String> getColumns() {
        return columns;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

}
