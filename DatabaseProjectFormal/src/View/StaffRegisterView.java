package View;

import backcode.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StaffRegisterView extends JDialog implements ActionListener {
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    //
    JLabel name = new JLabel("姓名:");
    JTextField nameTxt = new JTextField();
    //
    JLabel usernameLabel = new JLabel("用户名：");
    JTextField userTxt = new JTextField();
    JLabel passwordLabel = new JLabel("密码：");
    JPasswordField pwdField = new JPasswordField();
    JLabel phoneNumber = new JLabel("电话：");
    JTextField phoneFiled = new JTextField();
    JButton registerBtn = new JButton("注册");
    JButton exitBtn = new JButton("取消");
    JRadioButton accountantBtn = new JRadioButton("会计");
    JRadioButton salesmanBtn = new JRadioButton("业务员");

    JPanel jPanel = new JPanel(new GridLayout(1, 2));
    ButtonGroup buttonGroup = new ButtonGroup();

    public StaffRegisterView() {
        beautifyJDialog();
        //
        centerPanel.add(name);
        centerPanel.add(nameTxt);
        //
        centerPanel.add(usernameLabel);
        centerPanel.add(userTxt);
        centerPanel.add(passwordLabel);
        centerPanel.add(pwdField);
        centerPanel.add(phoneNumber);
        centerPanel.add(phoneFiled);
        centerPanel.add(registerBtn);
        centerPanel.add(exitBtn);

        buttonGroup.add(accountantBtn);
        buttonGroup.add(salesmanBtn);
        centerPanel.add(jPanel);
        jPanel.add(salesmanBtn);
        jPanel.add(accountantBtn);
        salesmanBtn.setSelected(true);

        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.setSize(600, 440);
        registerBtn.addActionListener(this);
        exitBtn.addActionListener(this);
        this.setTitle("注册界面");


        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    //
    public JTextField getUserTxt() {
        return userTxt;
    }

    public JPasswordField getPwdField() {
        return pwdField;
    }

    public JTextField getPhoneFiled() {
        return phoneFiled;
    }

    public JTextField getNameTxt() {
        return nameTxt;
    }
    //

    private void beautifyJDialog() {
        usernameLabel.setFont(new Font("楷体", Font.PLAIN, 20));
        userTxt.setPreferredSize(new Dimension(200, 30));
        //
        name.setFont(new Font("楷体", Font.PLAIN, 20));
        nameTxt.setPreferredSize(new Dimension(200, 30));
        //
        passwordLabel.setFont(new Font("楷体", Font.PLAIN, 20));
        pwdField.setPreferredSize(new Dimension(200, 30));
        phoneNumber.setFont(new Font("楷体", Font.PLAIN, 20));
        phoneFiled.setPreferredSize(new Dimension(200, 30));
        registerBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        exitBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        accountantBtn.setFont(new Font("楷体", Font.PLAIN, 15));
        salesmanBtn.setFont(new Font("楷体", Font.PLAIN, 15));
        //弹窗布局
        Spring childWidth = Spring.sum(Spring.sum(Spring.width(usernameLabel), Spring.width(userTxt)),
                Spring.constant(20));
        int offsetx = childWidth.getValue() / 2;

        springLayout.putConstraint(SpringLayout.WEST, usernameLabel, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, usernameLabel, 20, SpringLayout.NORTH, centerPanel);
        springLayout.putConstraint(SpringLayout.WEST, userTxt, 20, SpringLayout.EAST, usernameLabel);
        springLayout.putConstraint(SpringLayout.NORTH, userTxt, 0, SpringLayout.NORTH, usernameLabel);
        springLayout.putConstraint(SpringLayout.NORTH, passwordLabel, 40, SpringLayout.SOUTH, usernameLabel);
        springLayout.putConstraint(SpringLayout.EAST, passwordLabel, 0, SpringLayout.EAST, usernameLabel);
        springLayout.putConstraint(SpringLayout.WEST, pwdField, 20, SpringLayout.EAST, passwordLabel);
        springLayout.putConstraint(SpringLayout.NORTH, pwdField, 0, SpringLayout.NORTH, passwordLabel);
        springLayout.putConstraint(SpringLayout.NORTH, name, 40, SpringLayout.SOUTH, passwordLabel);
        springLayout.putConstraint(SpringLayout.EAST, name, 0, SpringLayout.EAST, passwordLabel);
        springLayout.putConstraint(SpringLayout.WEST, nameTxt, 20, SpringLayout.EAST, name);
        springLayout.putConstraint(SpringLayout.NORTH, nameTxt, 0, SpringLayout.NORTH, name);
        //
        springLayout.putConstraint(SpringLayout.NORTH, phoneNumber, 40, SpringLayout.SOUTH, name);
        springLayout.putConstraint(SpringLayout.EAST, phoneNumber, 0, SpringLayout.EAST, name);
        springLayout.putConstraint(SpringLayout.WEST, phoneFiled, 20, SpringLayout.EAST, phoneNumber);
        springLayout.putConstraint(SpringLayout.NORTH, phoneFiled, 0, SpringLayout.NORTH, phoneNumber);


//        springLayout.putConstraint(SpringLayout.WEST, kjJr,-offsetx, SpringLayout.HORIZONTAL_CENTER,centerPanel);
//        springLayout.putConstraint(SpringLayout.NORTH, kjJr,40,SpringLayout.SOUTH,passwordLabel);
//        springLayout.putConstraint(SpringLayout.EAST, ywJr,offsetx, SpringLayout.HORIZONTAL_CENTER,centerPanel);
//        springLayout.putConstraint(SpringLayout.NORTH, ywJr,40,SpringLayout.SOUTH,passwordLabel);

        springLayout.putConstraint(SpringLayout.NORTH, jPanel, 20, SpringLayout.SOUTH, phoneNumber);
        springLayout.putConstraint(SpringLayout.WEST, jPanel, -offsetx / 2, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.EAST, registerBtn, 0, SpringLayout.WEST, phoneFiled);
        springLayout.putConstraint(SpringLayout.NORTH, registerBtn, 40, SpringLayout.SOUTH, jPanel);
        springLayout.putConstraint(SpringLayout.EAST, exitBtn, offsetx, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, exitBtn, 40, SpringLayout.SOUTH, jPanel);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == registerBtn) {
            //点击注册后,选择对应添加到数据库
            String type = null;
            if (salesmanBtn.isSelected()) {
                type = Salesman.IDENTITY;
            } else if (accountantBtn.isSelected()) {
                type = Accountant.IDENTITY;
            }
            try {
                if(!register(type)){
                    this.dispose();
                    new StaffRegisterView();
                }else{
                    JOptionPane.showMessageDialog(this, "注册成功！");
                    this.dispose();
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        } else if (e.getSource() == exitBtn) {
            this.dispose();
        }
    }

    public boolean register(String type) throws Exception {
        String username = this.userTxt.getText();
        String password = new String(this.pwdField.getPassword());
        String name = this.nameTxt.getText();
        String phoneNumber = this.phoneFiled.getText();
        String idNumber = Tool.getIdNumber(type);
        if (DatabaseSystemLink.isExistedUsername(username, type)) {
            JOptionPane.showMessageDialog(this, "用户名已存在");
            return false;
        } else {
            Staff staff = null;
            switch (type) {
                case Accountant.IDENTITY -> {
                    staff = new Accountant(idNumber, name, phoneNumber, username, password);
                }
                case Salesman.IDENTITY -> {
                    staff = new Salesman(idNumber, name, phoneNumber, username, password);
                }
            }
            if (DatabaseSystemLink.saveStaffData(staff, type)) {
                return true;
            } else {
                return false;
            }
        }

    }
}