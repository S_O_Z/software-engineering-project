package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class SalesmanView extends JFrame implements ActionListener {
    JButton enterOrderBtn = new JButton("订单录入");
    JButton visualGoodBtn=new JButton("商品列表");
    JButton exitBtn = new JButton("返回");
    static SpringLayout springLayout = new SpringLayout();
    public static JPanel imgPanel=new JPanel(springLayout) {
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            ImageIcon icon = new ImageIcon("src/main/resources/img_1.png");
            Image imag = icon.getImage();
            g.drawImage(imag, 0, 0, icon.getIconWidth(), icon.getIconHeight(), icon.getImageObserver());
        }
    };

    public SalesmanView() {
        beutifyFrame();
        imgPanel.add(enterOrderBtn);
        imgPanel.add(exitBtn);
        imgPanel.add(visualGoodBtn);
        this.getContentPane().add(imgPanel);
        enterOrderBtn.addActionListener(this);
        exitBtn.addActionListener(this);
        visualGoodBtn.addActionListener(this);

        this.setSize(800, 600);
        this.setTitle("业务员界面");

        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);

    }

    private void beutifyFrame() {
        enterOrderBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        exitBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        visualGoodBtn.setFont(new Font("楷体", Font.PLAIN, 20));

        //布局
        int offsetx = Spring.width(exitBtn).getValue() / 2;
        springLayout.putConstraint(SpringLayout.WEST, enterOrderBtn, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, imgPanel);
        springLayout.putConstraint(SpringLayout.NORTH, enterOrderBtn, -100, SpringLayout.VERTICAL_CENTER, imgPanel);
        springLayout.putConstraint(SpringLayout.NORTH, visualGoodBtn, 30, SpringLayout.SOUTH, enterOrderBtn);
        springLayout.putConstraint(SpringLayout.WEST, visualGoodBtn, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, imgPanel);
        springLayout.putConstraint(SpringLayout.NORTH, exitBtn, 30, SpringLayout.SOUTH, visualGoodBtn);
        springLayout.putConstraint(SpringLayout.WEST, exitBtn, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, imgPanel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String name = ((JButton) e.getSource()).getText();
        switch (name) {
            case "订单录入":
                try {
                    new Consumer_JudgeDialog();
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                break;
            case "商品列表":
                try {
                    new Goods_ShowWithOperationView();
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                break;
            case "返回":
                this.dispose();
                new StaffLoginView();

                break;
        }
    }
}
