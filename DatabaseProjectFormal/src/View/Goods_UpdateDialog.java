package View;

import backcode.DatabaseSystemLink;
import backcode.Goods;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Goods_UpdateDialog extends JDialog implements ActionListener {
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    JLabel goodNo = new JLabel("商品编号：");
    JTextField goodNoTxt = new JTextField();
    JButton enter = new JButton("确认");
    JButton exitBtn = new JButton("取消");

    public Goods_UpdateDialog() {
        beautifyJDialog();
        centerPanel.add(goodNo);
        centerPanel.add(goodNoTxt);
        centerPanel.add(enter);
        centerPanel.add(exitBtn);


        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.setSize(600, 200);
        enter.addActionListener(this);
        exitBtn.addActionListener(this);
        this.setTitle("修改商品");

        this.setAlwaysOnTop(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    //
    public JTextField getGoodNoTxt() {
        return goodNoTxt;
    }
    //

    private void beautifyJDialog() {
        goodNo.setFont(new Font("楷体", Font.PLAIN, 20));
        goodNoTxt.setPreferredSize(new Dimension(200, 30));
        enter.setFont(new Font("楷体", Font.PLAIN, 20));
        exitBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        //弹窗布局
        Spring childWidth = Spring.sum(Spring.sum(Spring.width(goodNo), Spring.width(goodNoTxt)),
                Spring.constant(20));
        int offsetx = childWidth.getValue() / 2;

        springLayout.putConstraint(SpringLayout.WEST, goodNo, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, goodNo, 20, SpringLayout.NORTH, centerPanel);
        springLayout.putConstraint(SpringLayout.WEST, goodNoTxt, 20, SpringLayout.EAST, goodNo);
        springLayout.putConstraint(SpringLayout.NORTH, goodNoTxt, 0, SpringLayout.NORTH, goodNo);

        springLayout.putConstraint(SpringLayout.EAST, enter, 0, SpringLayout.WEST, goodNoTxt);
        springLayout.putConstraint(SpringLayout.NORTH, enter, 40, SpringLayout.SOUTH, goodNoTxt);
        springLayout.putConstraint(SpringLayout.EAST, exitBtn, offsetx, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, exitBtn, 40, SpringLayout.SOUTH, goodNoTxt);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == enter) {
            String id = this.getGoodNoTxt().getText();
            //点击后判断有无商品再做下一步修改
            try {
                if (DatabaseSystemLink.isExistedIdNumber(id, Goods.IDENTITY)) {
                    this.dispose();
                    new Update_Jdialog(id);
                } else {
                    JOptionPane.showMessageDialog(this, "商品不存在");
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        if (e.getSource() == exitBtn) {
            System.out.println("退出");
            this.dispose();
        }
    }
}

class Update_Jdialog extends JDialog implements ActionListener {
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    //
    JLabel price = new JLabel("价格: ");
    JTextField priceTxt = new JTextField();

    JLabel goodName = new JLabel("商品名：");
    JTextField nameField = new JTextField();
    JLabel amount = new JLabel("数量：");
    JTextField amountFiled = new JTextField();
    JButton Add = new JButton("更新");
    JButton exitBtn = new JButton("取消");
    boolean status = false;
    String id;

    public Update_Jdialog(String id) {
        beautifyJDialog();
        this.id = id;
        centerPanel.add(price);
        centerPanel.add(priceTxt);

        centerPanel.add(goodName);
        centerPanel.add(nameField);
        centerPanel.add(amount);
        centerPanel.add(amountFiled);
        centerPanel.add(Add);
        centerPanel.add(exitBtn);

        this.setAlwaysOnTop(true);
        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.setSize(600, 440);
        Add.addActionListener(this);
        exitBtn.addActionListener(this);
        this.setTitle("修改商品");

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public JTextField getNameField() {
        return nameField;
    }

    public JTextField getAmountFiled() {
        return amountFiled;
    }

    public JTextField getPriceTxt() {
        return priceTxt;
    }


    private void beautifyJDialog() {
        price.setFont(new Font("楷体", Font.PLAIN, 20));
        priceTxt.setPreferredSize(new Dimension(200, 30));
        goodName.setFont(new Font("楷体", Font.PLAIN, 20));
        nameField.setPreferredSize(new Dimension(200, 30));
        amount.setFont(new Font("楷体", Font.PLAIN, 20));
        amountFiled.setPreferredSize(new Dimension(200, 30));
        Add.setFont(new Font("楷体", Font.PLAIN, 20));
        exitBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        //弹窗布局
        Spring childWidth = Spring.sum(Spring.sum(Spring.width(goodName), Spring.width(nameField)),
                Spring.constant(20));
        int offsetx = childWidth.getValue() / 2;

        springLayout.putConstraint(SpringLayout.WEST, goodName, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, goodName, 20, SpringLayout.NORTH, centerPanel);
        springLayout.putConstraint(SpringLayout.WEST, nameField, 20, SpringLayout.EAST, goodName);
        springLayout.putConstraint(SpringLayout.NORTH, nameField, 0, SpringLayout.NORTH, goodName);
        springLayout.putConstraint(SpringLayout.NORTH, price, 40, SpringLayout.SOUTH, goodName);
        springLayout.putConstraint(SpringLayout.EAST, price, 0, SpringLayout.EAST, goodName);
        springLayout.putConstraint(SpringLayout.WEST, priceTxt, 20, SpringLayout.EAST, price);
        springLayout.putConstraint(SpringLayout.NORTH, priceTxt, 0, SpringLayout.NORTH, price);
        //
        springLayout.putConstraint(SpringLayout.NORTH, amount, 40, SpringLayout.SOUTH, price);
        springLayout.putConstraint(SpringLayout.EAST, amount, 0, SpringLayout.EAST, price);
        springLayout.putConstraint(SpringLayout.WEST, amountFiled, 20, SpringLayout.EAST, amount);
        springLayout.putConstraint(SpringLayout.NORTH, amountFiled, 0, SpringLayout.NORTH, amount);


        springLayout.putConstraint(SpringLayout.EAST, Add, 0, SpringLayout.WEST, amountFiled);
        springLayout.putConstraint(SpringLayout.NORTH, Add, 40, SpringLayout.SOUTH, amountFiled);
        springLayout.putConstraint(SpringLayout.EAST, exitBtn, offsetx, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, exitBtn, 40, SpringLayout.SOUTH, amountFiled);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Add) {
            String name = this.getNameField().getText();
            String price = this.getPriceTxt().getText();
            String amount = this.getAmountFiled().getText();
            Goods goods = new Goods(name, Double.parseDouble(price), Integer.parseInt(amount));
            try {
                if (DatabaseSystemLink.updateGoods(id, goods)) {
                    JOptionPane.showMessageDialog(this, "更新成功！");
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(this, "更新失败");
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        if (e.getSource() == exitBtn) {

            this.dispose();
        }
    }
}