package View;


import backcode.DatabaseSystemLink;
import backcode.Goods;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Goods_ReplenishDialog extends JDialog implements ActionListener {
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);

    JLabel goodNo = new JLabel("商品编号：");
    JTextField goodNoTxt = new JTextField();
    JLabel amount = new JLabel("添加的数量：");
    JTextField amountTxt = new JTextField();
    JButton Add = new JButton("添加");
    JButton exitBtn = new JButton("取消");
    public Goods_ReplenishDialog() {
        beautifyJDialog();
        centerPanel.add(goodNo);
        centerPanel.add(goodNoTxt);
        centerPanel.add(amount);
        centerPanel.add(amountTxt);
        centerPanel.add(Add);
        centerPanel.add(exitBtn);
        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.setSize(600, 440);
        Add.addActionListener(this);
        exitBtn.addActionListener(this);
        this.setTitle("补充商品");

        this.setAlwaysOnTop(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
    public JTextField getGoodNoTxt() {
        return goodNoTxt;
    }
    public JTextField getAmountTxt() {
        return amountTxt;
    }
    private void beautifyJDialog() {
        goodNo.setFont(new Font("楷体", Font.PLAIN, 20));
        goodNoTxt.setPreferredSize(new Dimension(200, 30));
        amount.setFont(new Font("楷体", Font.PLAIN, 20));
        amountTxt.setPreferredSize(new Dimension(200, 30));
        Add.setFont(new Font("楷体", Font.PLAIN, 20));
        exitBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        //弹窗布局
        Spring childWidth = Spring.sum(Spring.sum(Spring.width(goodNo), Spring.width(goodNoTxt)),
                Spring.constant(20));
        int offsetX = childWidth.getValue() / 2;
        springLayout.putConstraint(SpringLayout.WEST, goodNo, -offsetX,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, goodNo, 20, SpringLayout.NORTH, centerPanel);
        springLayout.putConstraint(SpringLayout.WEST, goodNoTxt, 20, SpringLayout.EAST, goodNo);
        springLayout.putConstraint(SpringLayout.NORTH, goodNoTxt, 0, SpringLayout.NORTH, goodNo);

        springLayout.putConstraint(SpringLayout.NORTH, amount, 40, SpringLayout.SOUTH, goodNoTxt);
        springLayout.putConstraint(SpringLayout.EAST, amount, 0, SpringLayout.EAST, goodNo);
        springLayout.putConstraint(SpringLayout.WEST, amountTxt, 20, SpringLayout.EAST, amount);
        springLayout.putConstraint(SpringLayout.NORTH, amountTxt, 0, SpringLayout.NORTH, amount);


//        springLayout.putConstraint(SpringLayout.WEST, kjJr,-offsetX, SpringLayout.HORIZONTAL_CENTER,centerPanel);
//        springLayout.putConstraint(SpringLayout.NORTH, kjJr,40,SpringLayout.SOUTH,passwordLabel);
//        springLayout.putConstraint(SpringLayout.EAST, ywJr,offsetX, SpringLayout.HORIZONTAL_CENTER,centerPanel);
//        springLayout.putConstraint(SpringLayout.NORTH, ywJr,40,SpringLayout.SOUTH,passwordLabel);

        springLayout.putConstraint(SpringLayout.EAST, Add, 0, SpringLayout.WEST, amountTxt);
        springLayout.putConstraint(SpringLayout.NORTH, Add, 40, SpringLayout.SOUTH, amountTxt);
        springLayout.putConstraint(SpringLayout.EAST, exitBtn, offsetX, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, exitBtn, 40, SpringLayout.SOUTH, amountTxt);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Add) {
            //点击添加后,选择对应添加到数据库
            String id = this.getGoodNoTxt().getText();
            String amount = this.getAmountTxt().getText();
            try {
                if(DatabaseSystemLink.isExistedIdNumber(id, Goods.IDENTITY)){
                    DatabaseSystemLink.replenishment(id,Integer.parseInt(amount));
                    JOptionPane.showMessageDialog(this, "补货成功！");
                    this.dispose();
                }else{
                    JOptionPane.showMessageDialog(this, "货物不存在！");
                    this.dispose();
                    new Goods_ReplenishDialog();
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }

        }
        if (e.getSource() == exitBtn) {
            this.dispose();
        }
    }
}

