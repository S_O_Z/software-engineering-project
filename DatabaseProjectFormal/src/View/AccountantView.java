package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AccountantView extends JFrame implements ActionListener {
    JButton visualCustomBtn = new JButton("客户");
    JButton sendOrderBtn = new JButton("订单");
    JButton exitBtn = new JButton("返回");
    static SpringLayout springLayout = new SpringLayout();
    //添加背景
    public static JPanel imgPanel=new JPanel(springLayout) {
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            ImageIcon icon = new ImageIcon("src/main/resources/img_1.png");
            Image imag = icon.getImage();
            g.drawImage(imag, 0, 0, icon.getIconWidth(), icon.getIconHeight(), icon.getImageObserver());
        }
    };

    public AccountantView() {
        beautifyFrame();
        initFrame();


    }

    private void initFrame() {
        this.add(imgPanel);
        imgPanel.add(sendOrderBtn);
        imgPanel.add(visualCustomBtn);
        imgPanel.add(exitBtn);
        this.getContentPane().add(imgPanel);
        sendOrderBtn.addActionListener(this);
        visualCustomBtn.addActionListener(this);
        exitBtn.addActionListener(this);

        this.setSize(800, 600);
        this.setTitle("会计界面");


        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    private void beautifyFrame() {
        visualCustomBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        sendOrderBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        exitBtn.setFont(new Font("楷体", Font.PLAIN, 20));

        //布局
        int offsetx = Spring.width(exitBtn).getValue() / 2;
        springLayout.putConstraint(SpringLayout.WEST, sendOrderBtn, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, imgPanel);
        springLayout.putConstraint(SpringLayout.NORTH, sendOrderBtn, -offsetx, SpringLayout.VERTICAL_CENTER, imgPanel);
        springLayout.putConstraint(SpringLayout.SOUTH, visualCustomBtn, -30, SpringLayout.NORTH, sendOrderBtn);
        springLayout.putConstraint(SpringLayout.WEST, visualCustomBtn, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, imgPanel);
        springLayout.putConstraint(SpringLayout.NORTH, exitBtn, 30, SpringLayout.SOUTH, sendOrderBtn);
        springLayout.putConstraint(SpringLayout.WEST, exitBtn, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, imgPanel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String name = ((JButton) e.getSource()).getText();
        switch (name) {
            case "客户":
                try {
                    new Consumer_ShowWithOperation();
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                break;
            case "订单":
                try {
                    new Order_SendDialog();
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                break;
            case "返回":
                this.dispose();
                new StaffLoginView();

                break;
        }
    }
}

