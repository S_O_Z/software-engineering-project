package View;


import backcode.DatabaseSystemLink;
import backcode.Goods;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Order_AddDialog extends JDialog implements ActionListener {
    SpringLayout springLayout = new SpringLayout();
    JPanel centerPanel = new JPanel(springLayout);
    JLabel goodNo = new JLabel("商品编号：");
    JTextField goodNoTxt = new JTextField();
    JLabel amount = new JLabel("数量：");
    JTextField amountTxt = new JTextField();
    JButton Add = new JButton("添加");
    JButton exitBtn = new JButton("取消");
    boolean status = false;

    String conId;
    String orderId;

    public Order_AddDialog(String conId, String orderId) {
        this.conId = conId;
        this.orderId = orderId;
        beautifyJDialog();
        centerPanel.add(goodNo);
        centerPanel.add(goodNoTxt);
        centerPanel.add(amount);
        centerPanel.add(amountTxt);
        centerPanel.add(Add);
        centerPanel.add(exitBtn);

        this.setAlwaysOnTop(true);
        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.setSize(600, 300);
        Add.addActionListener(this);
        exitBtn.addActionListener(this);
        this.setTitle("添加商品");


        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public JTextField getGoodNoTxt() {
        return goodNoTxt;
    }


    public JTextField getAmountTxt() {
        return amountTxt;
    }


    private void beautifyJDialog() {
        goodNo.setFont(new Font("楷体", Font.PLAIN, 20));
        goodNoTxt.setPreferredSize(new Dimension(200, 30));
        amount.setFont(new Font("楷体", Font.PLAIN, 20));
        amountTxt.setPreferredSize(new Dimension(200, 30));
        Add.setFont(new Font("楷体", Font.PLAIN, 20));
        exitBtn.setFont(new Font("楷体", Font.PLAIN, 20));
        //弹窗布局
        Spring childWidth = Spring.sum(Spring.sum(Spring.width(goodNo), Spring.width(goodNoTxt)),
                Spring.constant(20));
        int offsetx = childWidth.getValue() / 2;

        springLayout.putConstraint(SpringLayout.WEST, goodNo, -offsetx,
                SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, goodNo, 20, SpringLayout.NORTH, centerPanel);
        springLayout.putConstraint(SpringLayout.WEST, goodNoTxt, 20, SpringLayout.EAST, goodNo);
        springLayout.putConstraint(SpringLayout.NORTH, goodNoTxt, 0, SpringLayout.NORTH, goodNo);

        springLayout.putConstraint(SpringLayout.NORTH, amount, 40, SpringLayout.SOUTH, goodNo);
        springLayout.putConstraint(SpringLayout.EAST, amount, 0, SpringLayout.EAST, goodNo);
        springLayout.putConstraint(SpringLayout.WEST, amountTxt, 20, SpringLayout.EAST, amount);
        springLayout.putConstraint(SpringLayout.NORTH, amountTxt, 0, SpringLayout.NORTH, amount);

        springLayout.putConstraint(SpringLayout.EAST, Add, 0, SpringLayout.WEST, amountTxt);
        springLayout.putConstraint(SpringLayout.NORTH, Add, 40, SpringLayout.SOUTH, amountTxt);
        springLayout.putConstraint(SpringLayout.EAST, exitBtn, offsetx, SpringLayout.HORIZONTAL_CENTER, centerPanel);
        springLayout.putConstraint(SpringLayout.NORTH, exitBtn, 40, SpringLayout.SOUTH, amountTxt);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Add) {
            String goodsId = this.getGoodNoTxt().getText();
            int goodsAmount = Integer.parseInt(this.getAmountTxt().getText());
            try {
                if (DatabaseSystemLink.isExistedIdNumber(goodsId,Goods.IDENTITY)) {
                    try {
                        if (DatabaseSystemLink.isEnoughGoods(goodsId, goodsAmount)) {
                            try {
                                if (DatabaseSystemLink.reduceGoods(goodsId, goodsAmount) && DatabaseSystemLink.saveGoodsInOrder(orderId, goodsId, goodsAmount)) {
                                    JOptionPane.showMessageDialog(this, "添加成功！");
                                    this.dispose();
                                } else {
                                    JOptionPane.showMessageDialog(this, "添加失败！");
                                }
                            } catch (Exception ex) {
                                throw new RuntimeException(ex);
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "商品数量不足！");
                            new ShowGoods();
                        }
                    } catch (Exception ex) {
                        throw new RuntimeException(ex);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "商品不存在！");
                    new ShowGoods();
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
            if (true) {
                JOptionPane.showMessageDialog(this, "成功");
            } else if (false) {
                //如果商品数量不够展示商品
                try {
                    new Goods_ShowWithOperationView();
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            } else {
                JOptionPane.showMessageDialog(this, "商品编号有误请重新输入");
            }

        }
        if (e.getSource() == exitBtn) {
            System.out.println("退出");
            this.dispose();
        }
    }
}