package backcode;
//业务员
public class Salesman extends Staff {
    public final static String IDENTITY = "Salesman";

    public Salesman(String IdNumber, String staffName, String staffPhoneNumber, String username, String password) {
        super(IdNumber, staffName, staffPhoneNumber, username, password);
        setIdentity(IDENTITY);
    }

}
