package backcode;

public class Staff {
    private String idNumber;
    private String name;
    private String phoneNumber;
    private String username;
    private String password;
    private String identity;

    public Staff(String idNumber, String name, String phoneNumber) {
        this.idNumber=idNumber;
        this.name=name;
        this.phoneNumber=phoneNumber;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Staff(String IdNumber, String staffName, String staffPhoneNumber, String username, String password) {
        this.idNumber = IdNumber;
        this.name = staffName;
        this.phoneNumber = staffPhoneNumber;
        this.username = username;
        this.password = password;
    }

}
