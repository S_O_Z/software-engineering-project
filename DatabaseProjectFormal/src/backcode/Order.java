package backcode;

import java.util.ArrayList;

public class Order {
    public static final String IDENTITY="Order";
    public static String payByCash = "Cash";
    public static String payByCheque = "Cheque";
    public static String payByCreditCard = "Credit Card";
    private String orderIdNumber;
    private String payment;
    private boolean orderConditionSendAble;
    private ArrayList<Goods> goodsArrayList;

    public Order(String orderIdNumber, String payment, boolean orderConditionAffordable) {
        this.orderIdNumber = orderIdNumber;
        this.payment = payment;
    }

    public Order(String orderIdNumber) {
        this.orderIdNumber = orderIdNumber;
    }

    public boolean isOrderConditionSendAble() {
        return orderConditionSendAble;
    }

    public void setOrderConditionSendAble(boolean orderConditionSendAble) {
        this.orderConditionSendAble = orderConditionSendAble;
    }

    public String getOrderIdNumber() {
        return orderIdNumber;
    }

    public void setOrderIdNumber(String orderIdNumber) {
        this.orderIdNumber = orderIdNumber;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public ArrayList<Goods> getGoodsArrayList() {
        return goodsArrayList;
    }

    public void setGoodsArrayList(ArrayList<Goods> goodsArrayList) {
        this.goodsArrayList = goodsArrayList;
    }
}
