package backcode;

public class Goods {
    public final static String IDENTITY="Goods";
    private String goodsIdNumber;
    private String goodsName;
    private double goodsPrice;
    private int goodsAmount;

    public Goods(String goodsName, double goodsPrice, int goodsAmount) {
        this.goodsName = goodsName;
        this.goodsPrice = goodsPrice;
        this.goodsAmount = goodsAmount;
    }

    public Goods(String goodsIdNumber, String goodsName, double goodsPrice, int goodsAmount) {
        this.goodsIdNumber = goodsIdNumber;
        this.goodsName = goodsName;
        this.goodsPrice = goodsPrice;
        this.goodsAmount = goodsAmount;
    }

    public Goods(String goodsIdNumber, int goodsAmount) {
        this.goodsIdNumber = goodsIdNumber;
        this.goodsAmount = goodsAmount;
    }

    public Goods(String goodsIdNumber, String goodsName, int goodsAmount) {
        this.goodsIdNumber = goodsIdNumber;
        this.goodsName = goodsName;
        this.goodsAmount = goodsAmount;
    }

    public String getGoodsIdNumber() {
        return goodsIdNumber;
    }

    public void setGoodsIdNumber(String goodsIdNumber) {
        this.goodsIdNumber = goodsIdNumber;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public double getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public int getGoodsAmount() {
        return goodsAmount;
    }

    public void setGoodsAmount(int goodsAmount) {
        this.goodsAmount = goodsAmount;
    }
}
