package backcode;
import java.util.ArrayList;
public class Consumer {
    public final static String IDENTITY="Consumer";
    private String conIdNumber;
    private String conName;
    private String conPhoneNumber;
    private ArrayList<Order> orderArrayList;

    public Consumer(String conIdNumber, String conName, String conPhoneNumber) {
        this.conIdNumber = conIdNumber;
        this.conName = conName;
        this.conPhoneNumber = conPhoneNumber;
    }

    public String getConIdNumber() {
        return conIdNumber;
    }

    public void setConIdNumber(String conIdNumber) {
        this.conIdNumber = conIdNumber;
    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getConPhoneNumber() {
        return conPhoneNumber;
    }

    public void setConPhoneNumber(String conPhoneNumber) {
        this.conPhoneNumber = conPhoneNumber;
    }

    public ArrayList<Order> getOrderArrayList() {
        return orderArrayList;
    }

    public void setOrderArrayList(ArrayList<Order> orderArrayList) {
        this.orderArrayList = orderArrayList;
    }

}
