package backcode;

import java.util.Scanner;

public class Tool {

    static Scanner input = new Scanner(System.in);

    /**
     * 生成编号
     *
     * @param identity 身份
     * @return 工号/编号
     * @throws Exception 数据库连接异常
     */
    public static String getIdNumber(String identity) throws Exception {
        String idNumber;
        String idNumberFirst;
        String idNumberLast = null;
        idNumberFirst = DatabaseSystemLink.getFirstNumber(identity);
        int n = DatabaseSystemLink.getLastNumber(identity);
        if (n < 10) {
            idNumberLast = "00" + n;
        } else if (n > 10 && n < 100) {
            idNumberLast = "0" + n;
        } else if (n > 100 && n < 1000) {
            idNumberLast = "" + n;
        }
        idNumber = idNumberFirst + idNumberLast;
        DatabaseSystemLink.updateLastNumber(identity);
        while (DatabaseSystemLink.isExistedIdNumber(idNumber, identity)) {
            idNumber = getIdNumber(identity);
        }
        return idNumber;
    }

    /**
     * 生成订单编号
     * @param id 客户编号
     * @return 订单编号
     * @throws Exception 数据库连接异常
     */
    public static String getOrderIdNumber(String id) throws Exception {
        String idOrder ;
        int lastId = DatabaseSystemLink.getOrderAmount(id);
        if (lastId < 10) {
            idOrder = id + "00" + lastId;
        } else if (lastId < 100 && lastId > 10) {
            idOrder = id + "0" + lastId;
        } else {
            idOrder = id + lastId;
        }
        if(DatabaseSystemLink.isExistedOrder(idOrder)){
            idOrder=getIdNumber(Order.IDENTITY);
        }
        DatabaseSystemLink.updateOrderAmount(id);
        return idOrder;
    }

}
