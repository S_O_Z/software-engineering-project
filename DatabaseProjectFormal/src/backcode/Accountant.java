package backcode;

import java.util.Scanner;

//会计
public class Accountant extends Staff {

    public final static String IDENTITY ="Accountant";

    public Accountant(String IdNumber, String staffName, String staffPhoneNumber, String username, String password) {
        super(IdNumber, staffName, staffPhoneNumber, username, password);
    }

    public Accountant(String acIdNumber, String acName, String acPhoneNumber) {
        super(acIdNumber,acName,acPhoneNumber);
    }
}

